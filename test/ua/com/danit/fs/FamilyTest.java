package ua.com.danit.fs;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    @Test
    void testToString() {
        //   - `toString` - проверьте, что методы возвращает определенную строку для
        //   определенных объектов;
        Man father = new Man("Peter", "Yacobs", 1958);
        Woman mother = new Woman("Elisa", "Jonson", 1986);
        Man sun = new Man("Bill", "Yacobs", 2000);
        DomesticCat cat = new DomesticCat("Tomas", 7, 54, new HashSet<>());
        Collections.addAll(cat.getHabits(), "Sleep", "Eat", "Wolk");

        Family family = new Family(mother, father);

        family.addChild(sun);
        family.getPets().add(cat);
        String expected = "Family {Mother: Human {name = Elisa, surname = Jonson, year = 1986, iq = 0, Father: Human {name = Peter, surname = Yacobs, year = 1958, iq = 0, Children: Human {name = Bill, surname = Yacobs, year = 2000, iq = 0, , pets = [DOMESTIC_CAT {nickname = Tomas, age = 7, trickLevel = 54, habits = [Sleep, Eat, Wolk,  ]}]}";
        assertEquals(expected, family.toString());
    }

    @Test
    void testAddChild() {
        //  - `addChild` - проверьте, что массив `children` увеличивается на один элемент и что этим элементом
        //  есть именно переданный объект с необходимыми ссылками;
        Man father = new Man("Peter", "Yacobs", 1958);
        Woman mother = new Woman("Elisa", "Jonson", 1986);
        Man sun = new Man("Bill", "Yacobs", 2000);
        Family family = new Family(mother, father);
        family.addChild(sun);
        int result = family.getChildren().size();
        Human child = family.getChildren().get(family.getChildren().size() - 1);
        assertEquals(1, result);
        assertEquals(sun, child);
    }

    @Test
    void testRemoveChild() {
//  - проверьте, что ребенок действительно удаляется из массива `children`
//  (если передать объект, эквивалентный хотя бы одному элементу массива);
//  - проверьте, что массив `children` остается без изменений (если передать объект,
//  не эквивалентный ни одному элементу массива);
        Man father = new Man("Peter", "Yacobs", 1958);
        Woman mother = new Woman("Elisa", "Jonson", 1986);
        Man olderSun = new Man("Bill", "Yacobs", 2000);
        Man youngerSun = new Man("Bob", "Yacobs", 2003);
        Woman daughter = new Woman("Lisa", "Yacobs", 1998);
        Man nephew = new Man("Tom", "Robson", 1999);
        Family family = new Family(mother, father);
        family.addChild(olderSun);
        family.addChild(youngerSun);
        family.addChild(daughter);
        family.removeChild(daughter);

        int result = family.getChildren().size();
        assertEquals(2, result);

        family.removeChild(nephew);
        int res = family.getChildren().size();
        assertEquals(2, res);

    }

    @Test
    void testCountFamily() {
        //  - `countFamily` - проверьте, что метод возвращает верное количество человек в семье.
        Man father = new Man("Peter", "Yacobs", 1958);
        Woman mother = new Woman("Elisa", "Jonson", 1986);
        Man olderSun = new Man("Bill", "Yacobs", 2000);
        Man youngerSun = new Man("Bob", "Yacobs", 2003);
        Woman daughter = new Woman("Lisa", "Yacobs", 1998);
        Man nephew = new Man("Tom", "Robson", 1999);
        Family family = new Family(mother, father);
        family.addChild(olderSun);
        family.addChild(youngerSun);
        family.addChild(daughter);
        int result = family.countFamily();
        assertEquals(5, result);
    }
}