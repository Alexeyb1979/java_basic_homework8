package ua.com.danit.fs;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public abstract class Pet {
    private Species species = Species.UNKNOWN; //вид животного
    private String nickname; // кличка
    private int age; // возраст
    private int trickLevel; // уровень хитрости (целое число от 0 до 100)
    private Set<String> habits = new HashSet<String>(); //привычки


    public Pet(String nickname) {
        this.nickname = nickname.substring(0, 1).toUpperCase() + nickname.substring(1).toLowerCase();

    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this(nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {

    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    void eat() { //выводит на экран сообщение `Я кушаю!`
        System.out.println("Я кушаю!");
    }

    public abstract void respond();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age);
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();
        StringBuilder stringBuilder = new StringBuilder();
        for (String habit : this.habits) {
            stringBuilder.append(habit + ", ");
        }
        String petHabits = stringBuilder.toString();

        result.append(this.species);
        result.append(" {nickname = ");
        result.append(this.nickname);
        result.append(", age = ");
        result.append(this.age);
        result.append(", trickLevel = ");
        result.append(this.trickLevel);
        result.append(", habits = [");
        result.append(petHabits);
        result.append(" ]}");
        return result.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString() + "destroyed");
    }

    //Создайте enum Species со списком видов животных; добавьте перечисления на свое усмотрение;
    // сделайте рефакторинг класса Pet - вид животного изменится с типа String на тип Species

    enum Species {
        DOG,
        DOMESTIC_CAT,
        FISH,
        ROBO_CAT,
        UNKNOWN
    }
}
