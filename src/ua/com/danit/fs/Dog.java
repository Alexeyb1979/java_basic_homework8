package ua.com.danit.fs;

import java.util.HashSet;

public class Dog extends Pet implements Foul {

    {
        this.setSpecies(Species.DOG);
    }

    public Dog() {
    }

    public Dog(String nickname) {
        this.setNickname(nickname);
    }

    public Dog(String nickname, int age, int trickLevel,  HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
    }




    @Override
        public void respond () { //метод отозваться
            System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }

    @Override
        public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

}
