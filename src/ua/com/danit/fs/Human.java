package ua.com.danit.fs;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map <String, String> schedule = new HashMap<String, String>();

    enum DayOfWeek {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }

    private Family family;
    
    Human(String name, String surname, int year) {
        this.name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.schedule = schedule;
    }


     Human(){

     }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getSchedule() {
        schedule.put(String.valueOf(DayOfWeek.MONDAY), null);
        schedule.put(String.valueOf(DayOfWeek.TUESDAY), null);
        schedule.put(String.valueOf(DayOfWeek.WEDNESDAY), null);
        schedule.put(String.valueOf(DayOfWeek.THURSDAY), null);
        schedule.put(String.valueOf(DayOfWeek.FRIDAY), null);
        schedule.put(String.valueOf(DayOfWeek.SATURDAY), null);
        schedule.put(String.valueOf(DayOfWeek.SUNDAY), null);
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }



    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    DayOfWeek[] day = DayOfWeek.values();

//    public static void createSchedule(String [][] schedule){
//       for (int i = 0; i < schedule.length; i++) {
//            schedule[i][0] = String.valueOf(DayOfWeek.values()[i]);
//                System.out.print(schedule[i][j] = String.valueOf(Day.values()[i]);

//            System.out.println();

//    }

    public void greetPet (String name){
        this.name = name;
        System.out.println("Hello, " + name + "!");
    };

    @Override
    public String toString(){

       return "Human {name = " + this.name + ", surname = " + this.surname + ", year = " + this.year +
               ", iq = " + this.iq;
   }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, family);
    }

    //- Переопределите метод `finalize()` у классов `Family`, `Human`, `Pet` так, чтобы они перед удалением
    // сборщиком мусора вывводили в консоль информацию об удаляемом объекте.


    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString() + "destroyed");
    }
}
