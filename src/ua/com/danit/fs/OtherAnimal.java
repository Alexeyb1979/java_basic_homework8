package ua.com.danit.fs;

import java.util.HashSet;

public class OtherAnimal extends Pet {
    {
        this.setSpecies(Species.UNKNOWN);
    }

    public OtherAnimal(String nickname) {
        super(nickname);

    }

    public OtherAnimal(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);

    }

    public OtherAnimal() {

    }

       @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }
}
