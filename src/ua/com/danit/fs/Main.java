package ua.com.danit.fs;

import java.util.*;

public class Main {
    public static void main(String[] args) {

     Dog dog = new Dog ("Jack", 8, 43, new HashSet<>());

        Collections.addAll(dog.getHabits(), "Sleep", "Eat", "Wolk");

        DomesticCat cat = new DomesticCat("Tomas", 7, 54, new HashSet<>());
        Collections.addAll(cat.getHabits(), "Sleep", "Eat", "Wolk");
//        cat.getHabits().add("Eat");
//        cat.getHabits().add("Wolk");


        Fish fish = new Fish("nemo");
        fish.getSpecies();
        Collections.addAll(fish.getHabits(), "Swim", "Eat");
        System.out.println(fish);

      OtherAnimal rabbit = new OtherAnimal();
        rabbit.setNickname("Banny");
        Collections.addAll(rabbit.getHabits(), "jump");
        System.out.println(rabbit);

        Man father = new Man("Peter", "Yacobs", 1958);
        System.out.println(father.getSchedule());

        Woman bobbiesWife = new Woman("Elisa", "Jonson", 1986);
        Man olderSun = new Man("Bill", "Yacobs", 2000);
        Man  youngerSun = new Man("Bob", "Yacobs", 2003);
        Woman daughter = new Woman("Lisa", "Yacobs", 1998);
        Man  nephew = new Man ("Tom", "Robson", 1999);
        Man  bobbiesNephew = new Man ();

        Family family = new Family(bobbiesWife, father);
        family.addChild(olderSun);
        family.addChild(youngerSun);
        family.addChild(daughter);

        family.getPets().add(cat);
        family.getPets().add(dog);
        family.getPets().add(fish);
        family.getPets().add(rabbit);
        System.out.println(family.toString());
        family.removeChild(daughter);
        family.removeChild(daughter);

        System.out.println(family.toString());

        family.removeChild(bobbiesNephew);
        System.out.println(family.toString());

//        Family[] families = new Family[200000];

//        -Xmx5M to receive OutOfMemoryError
//        for (int i = 0; i < 200000; i++) {
//            families[i] = new Family (bobbiesWife, father);
//        }
    }


}
