package ua.com.danit.fs;

import java.util.Set;

public class RoboCat extends Pet {
    {
        this.setSpecies(Species.ROBO_CAT);
    }

    public RoboCat(String nickname) {
        super(nickname);
        }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);

    }

    public RoboCat() {
    }

    @Override
    public void respond () { //метод отозваться
        System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }
}
