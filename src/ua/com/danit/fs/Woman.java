package ua.com.danit.fs;

import java.util.HashMap;

public final class Woman extends Human {

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, HashMap<String, String> schedule) {
        super(name, surname, year, schedule);
    }

    public Woman() {
    }

    public void makeup() {
        System.out.println(getName() + ", is doing make up!");
    }

    @Override
    public void greetPet(String name) {
        super.greetPet(name);
        System.out.println (name + ", Hello! I am glad to see you!");
    }
}
