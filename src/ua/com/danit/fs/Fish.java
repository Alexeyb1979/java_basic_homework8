package ua.com.danit.fs;

import java.util.Set;

public class Fish extends Pet{
    {
        this.setSpecies(Species.FISH);
    }
    public Fish(String nickname) {
        super(nickname);

    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }


    public Fish() {
    }

   @Override
    public void respond () { //метод отозваться
        System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }
}
