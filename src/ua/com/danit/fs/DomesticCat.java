package ua.com.danit.fs;

import java.util.HashSet;

public class DomesticCat extends Pet implements Foul{

    {
        this.setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, HashSet <String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public DomesticCat() {
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }


}
