package ua.com.danit.fs;

import java.util.*;

public class Family {
    private Woman mother = new Woman();
    private Man father = new Man();
    private List<Human> children = new ArrayList<Human>();
//    private Pet pet;
    private Set <Pet> pets = new HashSet<Pet>();

    public Woman getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

   public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    Family(Woman mother, Man father) {
        this.mother = mother;
        this.father = father;
        father.setFamily(this);
        mother.setFamily(this);
    }

//    void greetPet() {
//        System.out.println("Привет, " + this.pet.getNickname() + "!");
//    }
//
//    void describePet() {
//        String trick;
//        if (this.pet.getTrickLevel() > 50) {
//            trick = "очень хитрый";
//        } else {
//            trick = "почти не хитрый";
//        }
//
//        System.out.println("У меня есть" + this.pet.getSpecies() + ", ему " + this.pet.getAge() + " лет, он" + trick + ".");
//
//    }

    //добавить ребенка `addChild` (принимает тип `Human` и добавляет его в массив детей; добавляет ребенку
    // ссылку на текущую семью)

    public void addChild(Human child) {
        children.add(child);
    }


    public void removeChild(Human removedChild) {
            if (children.contains(removedChild)) {
                children.remove(removedChild);
            } else {
                return;
            }
    }

    
    public int countFamily() {
        int result = 2 + this.children.size();
        System.out.println("This family consist of " + result + " members.");
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) &&
                father.equals(family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        StringBuilder stringBuilder = new StringBuilder();

        for (Human child : this.getChildren()) {
            stringBuilder.append(child.toString() + ", ");
        }
        String childrenList = stringBuilder.toString();

        result.append("Family {Mother: ");
        result.append(this.mother.toString());
        result.append(", Father: ");
        result.append(this.father.toString());
        result.append(", Children: ");
        result.append(childrenList);
        result.append(", pets = ");
        result.append(this.pets.toString());
        result.append("}");
        return result.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString() + "destroyed");
    }
}





