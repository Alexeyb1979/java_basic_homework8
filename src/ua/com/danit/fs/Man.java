package ua.com.danit.fs;

import java.util.HashMap;

public final class Man extends Human{
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, HashMap<String, String> schedule) {
        super(name, surname, year, schedule);
    }

    public Man() {
    }

    public void repairCar() {
        System.out.println(getName() + ", is repairing car!");
    }

    @Override
    public void greetPet(String name) {
        super.greetPet(name);
        System.out.println (getName() + ", Hello!");
    }
}
